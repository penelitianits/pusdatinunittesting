# PusdatinUnitTesting


## PHP Unit 

### Installation

```
composer require phpunit/phpunit ^9
```

Create or edit `composer.json` file to this :

```
{
    "autoload": {
        "classmap": [
            "src/"
        ]
    },
    "require-dev": {
        "phpunit/phpunit": "^9"
    }
}
```


### Test 

To test ex: `test/user.php` , excecute in CLI Directory :

```
./vendor/bin/phpunit tests
```


## Sonarqube

### Installation

Download via https://www.sonarsource.com/products/sonarqube/downloads/





















