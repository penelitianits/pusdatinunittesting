<?php

/**
 * @group skip
 */

class ShoppingCartTest extends PHPUnit\Framework\TestCase
{
    private $cart;
    
    protected function setUp(): void
    {
        $this->cart = new App\ShoppingCart();
        $this->cart->addItem('item1', 2);
        $this->cart->addItem('item2', 1);
    }
    
    public function testAddItem()
    {
        $this->cart->addItem('item3', 3);
        $items = $this->cart->getItems();
        $this->assertEquals(3, count($items));
        $this->assertArrayHasKey('item3', $items);
        $this->assertEquals(3, $items['item3']);
    }
    
    public function testRemoveItem()
    {
        $this->cart->removeItem('item1');
        $items = $this->cart->getItems();
        $this->assertEquals(1, count($items));
        $this->assertArrayNotHasKey('item1', $items);
    }
    
    public function testGetTotal()
    {
        $this->assertEquals(30.0, $this->cart->getTotal());
    }
    
    public function testGetTotalWithEmptyCart()
    {
        $cart = new App\ShoppingCart();
        $this->assertEquals(0.0, $cart->getTotal());
    }
}

