<?php

use PHPUnit\Framework\TestCase;

class BankAccountTest extends TestCase
{
    private $account;
    
    // Arrange

    protected function setUp(): void
    {
        $this->account = new App\BankAccount();
    }
    
        public function testDeposit()
        {
            // Act
            $this->account->deposit(100);
            
            // Assert
            $this->assertEquals(100, $this->account->getBalance());
            $this->assertCount(1, $this->account->getTransactions());
            $this->assertEquals('deposit', $this->account->getTransactions()[0]['type']);
            $this->assertEquals(100, $this->account->getTransactions()[0]['amount']);
        }
    
    public function testWithdraw()
    {
        // Arrange
        $this->account->deposit(100);
        
        // Act
        $this->account->withdraw(50);
        
        // Assert
        $this->assertEquals(50, $this->account->getBalance());
        $this->assertCount(2, $this->account->getTransactions());
        $this->assertEquals('withdrawal', $this->account->getTransactions()[1]['type']);
        $this->assertEquals(50, $this->account->getTransactions()[1]['amount']);
    }
    
    public function testWithdrawWithInsufficientFunds()
    {
        // Arrange
        $this->account->deposit(100);
        
        // Assert
        $this->expectException(RuntimeException::class);
        
        // Act
        $this->account->withdraw(150);
    }
    
    public function testDepositWithNegativeAmount()
    {
        // Assert
        $this->expectException(InvalidArgumentException::class);
        
        // Act
        $this->account->deposit(-100);
    }
    
    public function testWithdrawWithNegativeAmount()
    {
        // Arrange
        $this->account->deposit(100);
        
        // Assert
        $this->expectException(InvalidArgumentException::class);
        
        /// Act
        $this->account->withdraw(-50);
    }
}

