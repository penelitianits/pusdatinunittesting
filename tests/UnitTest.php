<?php



class AddTest extends\PHPUnit\Framework\TestCase
{
    public function testAdd()
    {
        // Arrange
        $calculator = new App\Add;

        // Act
        $result = $calculator->tambah(6, 9);

        // Assert
        $this->assertEquals(15, $result);
    }
}


class RectangleTest extends PHPUnit\Framework\TestCase
{
    public function testGetArea()
    {
        // Arrange & Act
        $rectangle = new App\Rectangle(5, 10);

        // Assert
        $this->assertEquals(50, $rectangle->getArea());
    }
}




