<?php

class UserTest extends PHPUnit\Framework\TestCase
{
    private $user;
    
    protected function setUp(): void
    {
        $this->user = new App\User(1, "John Doe", "john.doe@example.com", password_hash("password", PASSWORD_DEFAULT));
    }
    
    public function testGetId()
    {
        $this->assertEquals(1, $this->user->getId());
    }
    
    public function testGetName()
    {
        $this->assertEquals("John Doe", $this->user->getName());
    }
    
    public function testGetEmail()
    {
        $this->assertEquals("john.doe@example.com", $this->user->getEmail());
    }
    
    public function testGetPassword()
    {
        $this->assertTrue(password_verify("password", $this->user->getPassword()));
    }
    
    public function testSetPassword()
    {
        $this->user->setPassword(password_hash("newpassword", PASSWORD_DEFAULT));
        $this->assertTrue(password_verify("newpassword", $this->user->getPassword()));
    }
    
    public function testValidatePassword()
    {
        $this->assertTrue($this->user->validatePassword("password"));
        $this->assertFalse($this->user->validatePassword("wrongpassword"));
    }
}


