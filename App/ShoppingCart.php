<?php

namespace App;

class ShoppingCart
{
    private $items = array();
    
    public function addItem($product, $quantity)
    {
        if (!isset($this->items[$product])) {
            $this->items[$product] = 0;
        }
        $this->items[$product] += $quantity;
    }
    
    public function removeItem($product)
    {
        if (!isset($this->items[$product])) {
            throw new Exception("Item not found.");
        }
        unset($this->items[$product]);
    }
    
    public function getItems()
    {
        return $this->items;
    }
    
    public function getTotal()
    {
        $total = 0;
        foreach ($this->items as $product => $quantity) {
            $total += $this->getPrice($product) * $quantity;
        }
        return $total;
    }
}

