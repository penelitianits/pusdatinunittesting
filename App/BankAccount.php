<?php

namespace App;

class BankAccount
{
    private $balance;
    private $transactions;
    
    public function __construct()
    {
        /*
        Inisialisasi Saldo dan Array untuk
        jenis dan nilai transaksi
        */

        $this->balance = 0;
        $this->transactions = [];
    }
    
    public function deposit($amount)
    {
        /*
        Melakukan penyetoran uang sebanyak $amount
        */

        if ($amount <= 0) {
            throw new InvalidArgumentException('Deposit amount must be greater than zero');
        }
        $this->balance += $amount;
        $this->transactions[] = ['type' => 'deposit', 'amount' => $amount];
    }
    
    public function withdraw($amount)
    {
        /*
        Melakukan penarikan uang sebanyak $amount
        */
        
        if ($amount <= 0) {
            throw new InvalidArgumentException('Withdrawal amount must be greater than zero');
        }
        if ($amount > $this->balance) {
            throw new RuntimeException('Insufficient funds');
        }
        $this->balance -= $amount;
        $this->transactions[] = ['type' => 'withdrawal', 'amount' => $amount];
    }
    
    public function getBalance()
    {
        return $this->balance;
    }
    
    public function getTransactions()
    {
        return $this->transactions;
    }
}

